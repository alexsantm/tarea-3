//
//  AddViewController.swift
//  Tarea2
//
//  Created by macbook on 1/29/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class AddPeliculasViewController: UIViewController {

    @IBOutlet weak var nombrePelicula: UITextField!
    @IBOutlet weak var descripcionPelicula: UITextView!
    @IBOutlet weak var fechaEstrenoPelicula: UIDatePicker!
    @IBOutlet weak var pickerCategoria: UIPickerView!
    @IBOutlet weak var imagenPelicula: UITextField!
    
    var arrayCategorias = [[String:String]]()
        var categoriaSelected = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataBase.checkAndCreateDatabase(withName: "PeliculasDB.db")
        arrayCategorias = (DataBase().ejecutarSelect("Select id, categoria from categoria where categoria != '' order by categoria") as? [[String:String]])!
    }
    
    @IBAction func grabarDatos(_ sender: Any) {

        // Formato de Fecha
        fechaEstrenoPelicula.datePickerMode = .date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YYYY"
        let strDate = dateFormatter.string(from: fechaEstrenoPelicula.date)
        let fechaPelicula = strDate
        
        //Imagen
        var imagenSet = ""
        if let imagenPelicula = self.imagenPelicula.text, imagenPelicula.isEmpty {
           imagenSet = "pelicula"
        } else {
           imagenSet = self.imagenPelicula.text!
        }
        
        guard
            let nomPelicula = self.nombrePelicula.text,
            let descPelicula = self.descripcionPelicula.text,
            let idCategoriaSelected = Int(categoriaSelected["id"]!)
            else { return }
        
        // Guadar datos:
        let queryInsertPelicula =
        "Insert into peliculas ('nombre','detalle', 'fecha', 'id_categoria', 'imagen') values ('\(nomPelicula)','\(descPelicula)','\(fechaPelicula)', '\(idCategoriaSelected)', '\(imagenSet)') "
        
        DataBase.shared()?.ejecutarInsert(queryInsertPelicula)
        showSimpleAlert()
        cleanFields()

    }
    
    func cleanFields(){
        self.nombrePelicula.text = ""
        self.descripcionPelicula.text = ""
        self.imagenPelicula.text = ""
    }
    
    func showSimpleAlert() {
        let alert = UIAlertController(title: "Super bien", message: "Pelicula añadida correctamente", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension AddPeliculasViewController:UIPickerViewDataSource, UIPickerViewDelegate{

        func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
        }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return arrayCategorias.count
       }
       
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            let categoria = arrayCategorias[row]["categoria"]
            return categoria!
       }
    
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            categoriaSelected = arrayCategorias[row]
        }
}
