//
//  DetallePeliculasViewController.swift
//  Tarea2
//
//  Created by macbook on 1/29/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class DetallePeliculasViewController: UIViewController {

    @IBOutlet weak var tituloPelicula: UILabel!
    @IBOutlet weak var detallePelicula: UITextView!
    @IBOutlet weak var fechaEstrenoPelicula: UILabel!
    
    var paramReceptor:[String:String]?
    var arrayPeliculas = [Any]()
    
    var nombrePelicula: String = ""
    var descripcionPelicula: String = ""
    var fechaPelicula: String = ""
    var imagenPelicula: String = ""
    var categoriaPelicula: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataBase.checkAndCreateDatabase(withName: "PeliculasDB.db")
        
        guard let paramDatos = paramReceptor else {return}
        nombrePelicula = paramDatos["nombre"] ?? ""
        descripcionPelicula = paramDatos["detalle"]  ?? ""
        fechaPelicula = paramDatos["fecha"] ?? ""
        imagenPelicula = paramDatos["imagen"]  ?? ""
        categoriaPelicula = paramDatos["id_categoria"]  ?? "0"
   
        self.tituloPelicula.text = nombrePelicula
        self.detallePelicula.text = descripcionPelicula
        self.fechaEstrenoPelicula.text = fechaPelicula

    }

    @IBAction func addFavoritos(_ sender: Any) {
        
        // Guadar datos:
        let queryInsertFavoritos = "Insert into favoritos ('nombre','detalle', 'fecha', 'id_categoria') values ('\(nombrePelicula)','\(descripcionPelicula)','\(fechaPelicula)', '\(categoriaPelicula)') "
        
        DataBase.shared()?.ejecutarInsert(queryInsertFavoritos)
        showSimpleAlert()

    }
    
    func showSimpleAlert() {
        let alert = UIAlertController(title: "Super bien", message: "Pelicula añadida a Favoritos correctamente", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
