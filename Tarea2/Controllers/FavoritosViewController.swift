//
//  FavoritosViewController.swift
//  Tarea3
//
//  Created by macbook on 2/6/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class FavoritosViewController: UIViewController {
        
    @IBOutlet weak var FavoritosTableView: UITableView!
    
    var arrayPeliculas = [[String:String]]()
        var arrayCategorias = [[String:String]]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            DataBase.checkAndCreateDatabase(withName: "PeliculasDB.db")
        }
        
        override func viewWillAppear(_ animated: Bool) {
            arrayPeliculas = (DataBase().ejecutarSelect("Select * from favoritos")  as? [[String:String]])!
            arrayCategorias = (DataBase().ejecutarSelect("Select * from categoria")  as? [[String:String]])!
            FavoritosTableView.reloadData()
        }
        
//         override func prepare(for segue: UIStoryboardSegue, sender: Any?){
//            if segue.identifier == "goToDetalle"{
//                guard let indexSelected = PeliculasTableView.indexPathForSelectedRow else {return}
//                let objDetalleDestino = segue.destination as? DetallePeliculasViewController
//
//                objDetalleDestino?.paramReceptor = arrayPeliculas[indexSelected.row]
//            }
//        }
        
        func returnFilasPorCategoria(seccionNum:Int)->[[String:String]]{
            let idCategoria = seccionNum + 1
            let queryPeliculas = "Select * from favoritos where id_categoria = '\(idCategoria)' "
            if let peliculas =  DataBase().ejecutarSelect(queryPeliculas) as? [[String:String]] {
                return peliculas
            }else {
                return [[String:String]]()
            }
        }
        
    }

    extension FavoritosViewController:UITableViewDelegate, UITableViewDataSource{
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return arrayCategorias.count
        }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            let peliculasTemp = returnFilasPorCategoria(seccionNum: section)
            return peliculasTemp.count
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let peliculasTemp = returnFilasPorCategoria(seccionNum: indexPath.section)
            let cell = tableView.dequeueReusableCell(withIdentifier: "celdaFavoritos", for: indexPath)
            let nombrePelicula = peliculasTemp[indexPath.row]["nombre"] ??  ""
            let fechaPelicula = peliculasTemp[indexPath.row]["fecha"] ??  "0000-00-00"
            let descripcionPelicula = peliculasTemp[indexPath.row]["detalle"] ??  ""
            
              cell.textLabel?.text = nombrePelicula
              cell.detailTextLabel?.text = fechaPelicula
              cell.imageView?.image = UIImage(named: "pelicula")
              return cell
        }

        private func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> String? {
            return "seccion No. \(section)"
        }
        
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            guard let categoria = arrayCategorias[section]["categoria"] else { return "No seccion"}
            return categoria
        }

    }

