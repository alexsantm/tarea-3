//
//  ViewController.swift
//  Tarea2
//
//  Created by macbook on 1/29/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class PeliculasViewController: UIViewController {

    @IBOutlet weak var PeliculasTableView: UITableView!
    
    var arrayPeliculas = [[String:String]]()
    var arrayCategorias = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataBase.checkAndCreateDatabase(withName: "PeliculasDB.db")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //arrayPeliculas = (DataBase().ejecutarSelect("Select * from peliculas")  as? [[String:String]])!
        arrayCategorias = (DataBase().ejecutarSelect("Select * from categoria")  as? [[String:String]])!
        PeliculasTableView.reloadData()
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "goToDetalle"{
           guard let indexSelected = PeliculasTableView.indexPathForSelectedRow else {return}
           let peliculasTemp = returnFilasPorCategoria(seccionNum: indexSelected.section)
           let objDetalleDestino = segue.destination as? DetallePeliculasViewController
           objDetalleDestino?.paramReceptor = peliculasTemp[indexSelected.row]
        }
    }
    
    func returnFilasPorCategoria(seccionNum:Int)->[[String:String]]{
        let idCategoria = seccionNum + 1
        let queryPeliculas = "Select * from peliculas where id_categoria = '\(idCategoria)' "
        if let peliculas =  DataBase().ejecutarSelect(queryPeliculas) as? [[String:String]] {
            return peliculas
        }else {
            return [[String:String]]()
        }
    }
    
}

extension PeliculasViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayCategorias.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let peliculasTemp = returnFilasPorCategoria(seccionNum: section)
        return peliculasTemp.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let peliculasTemp = returnFilasPorCategoria(seccionNum: indexPath.section)
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaPelicula", for: indexPath)
        let nombrePelicula = peliculasTemp[indexPath.row]["nombre"] ??  ""
        let fechaPelicula = peliculasTemp[indexPath.row]["fecha"] ??  "0000-00-00"
        let descripcionPelicula = peliculasTemp[indexPath.row]["detalle"] ??  ""
        let imagenPelicula = peliculasTemp[indexPath.row]["imagen"] ??  "pelicula"
        print("Imagen: ", imagenPelicula)
        
          cell.textLabel?.text = nombrePelicula
          cell.detailTextLabel?.text = fechaPelicula
          //cell.imageView?.image = UIImage(named: "pelicula")
          cell.imageView?.image = UIImage(named: imagenPelicula)
          return cell
    }

    private func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> String? {
        return "seccion No. \(section)"
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let categoria = arrayCategorias[section]["categoria"] else { return "No seccion"}
        return categoria
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
//        //PeliculasTableView.deselectRow(at: indexPath, animated: true)
//        performSegue(withIdentifier:  "goToDetalle", sender: self)
//    }

}

